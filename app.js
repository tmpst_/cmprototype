var browserSync = require('browser-sync').create();
var middleware  = require('connect-gzip-static')('./public');

browserSync.init({
  server: 'public',
  files: ['public/**/*']
}, function(err, bs) {
  bs.addMiddleware('*', middleware, {
    override: true
  });
});
