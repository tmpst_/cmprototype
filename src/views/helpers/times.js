'use strict';

module.exports = function(n, id, domain, options) {
  var render = '';
  for (var i = 1; i <= n; ++i) {
    render += options.fn(i).replace('big/', 'big/' + id).replace('cdn.', 'cdn.' + domain);
  }

  return render;
}
