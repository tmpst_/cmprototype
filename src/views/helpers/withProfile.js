'use strict';

module.exports = function(id, options) {
  var profiles = options.data.root.profiles;
  var profile = {};
  // console.log(id, profiles);
  profiles.forEach(function(item){
    if (id == item.id) {
      profile = item;
    }
  })
  return profile;
};
