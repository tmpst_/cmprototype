var fs = require('fs');
var extend = require('extend');
var consolidate = require('consolidate');
var path = require('path');

var options = {
  engine: 'handlebars',
  directory: 'src/views/partials'
}

module.exports = function(name, params) {
  var file = path.resolve(options.directory, name);
  var partialContents = fs.readFileSync(file).toString('utf8');
  var context = params ? extend({}, params, this) : extend({}, this);

  var result = null;
  consolidate[options.engine].render(partialContents, context, function(error, rendered) {
    if (error) {
      throw new Error(error);
    }

    result = rendered;
  });

  return result;
};
