'use strict';

var moment = require('moment');
console.log('loaded');
for (var i = -1, nodes = document.getElementsByClassName('date-relative'), node; (node = nodes[++i]);) {
  var content = moment(node.getAttribute('datetime')).fromNow();
  node.textContent = content;

  // console.log('node', node, content);
}
