 // Submit forms in modals via ajax
 // replace the modal content with the server response
 // The server response need to be marked up as a modal

var request = require('./request');
var modal = require('./modal');
var notices = require('./notices');

document.addEventListener('submit', function(e) {
  var form = e.target;
  var modalcontent = form.closest('.modal');

  if (modalcontent) {
    e.preventDefault();

    request
      .post(form.action, new FormData(form))
      .then(function(res) {
        var content = new DOMParser()
          .parseFromString(res, 'text/html')
          .querySelector('.modal');

        modalcontent.parentNode.replaceChild(content, modalcontent);
      })
      .catch(function(err) {
        console.log(err);
        modal.destroy();
        notices.setError();
      });
  }
});
