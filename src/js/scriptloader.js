/**
 Batch load external scripts in parallel

   var loader = require('scriptloader');
   loader.add('path/to/script-1.js');
   loader.add('path/to/script-2.js');

   // Apply callback once both scripts have been loaded
   loader.loaded(callback);

 */

var seen = [];
var queue = [];

exports.add = function(url) {
  if (!~seen.indexOf(url)) {
    var promise = new Promise(function(resolve, reject) {

      var script = document.createElement('script');
      script.src = url;

      script.addEventListener('load', function() {
        seen.push(url);
        resolve(script);
      }, false);

      script.addEventListener('error', function() {
        reject(script);
      }, false);

      document.body.appendChild(script);

    });

    queue.push(promise);
  }
};

exports.loaded = function(callback) {
  Promise.all(queue).then(callback);
};
