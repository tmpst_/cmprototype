var notices = require('./notices');

// @TODO: support for blob and file?
var encodingMap = {
  formData: 'multipart/form-data; charset=UTF-8',
  formEncoded: 'application/x-www-form-urlencoded; charset=UTF-8',
  json: 'application/json; charset=UTF-8',
};

function onload(req, resolve, reject) {
  var status = req.status;
  var location = req.getResponseHeader('Location') || (function() {
    return (/redirectLink/.test(req.response))
      ? (status = 302, JSON.parseFromString(req.response).redirectLink)
      : '';
  })();

  if (status == 200) resolve(req.response);
  else if (status == 302) window.location.href = location;
  else {
    notices.setError(req.message || '');
    reject(new Error('Server returned error ' + status + ': ' + req.statusText));
  }
}

function onerror(reject) {
  reject(new Error('Connection error'));
}

exports.get = function(url, format) {
  return new Promise(function(resolve, reject) {
    var req = new XMLHttpRequest();

    req.open('GET', url);
    req.setRequestHeader('Accept', 'application/vnd.cupidmedia.' + (format || 'html'));
    req.send();

    req.onload = function() {
      onload(req, resolve, reject);
    };

    req.onerror = function() {
      onerror(reject);
    };
  });
};

exports.post = function(url, data, encoding, format) {
  return new Promise(function(resolve, reject) {
    var req = new XMLHttpRequest();

    req.open('POST', url);
    req.setRequestHeader('Accept', 'application/vnd.cupidmedia.' + (format || 'html'));
    req.setRequestHeader('Content-Type',  encodingMap[encoding || 'formData' ]);
    req.send(data);

    req.onload = function() {
      onload(req, resolve, reject);
    };

    req.onerror = function() {
      onerror(reject);
    };
  });
};
