var modal = require('./modal');
var request = require('./request');
var populate = require('./populate');
var notices = require('./notices');

var template = document.getElementById('mutualInterestTpl');

var queue = [];

var datamap = {
  name: ['.name', 'textContent'],
  avatar: ['.interest', 'src'],
  avatardefault: ['.card-avatar', 'class'], // avatar-{{ f || m }}
  msgbuttonlabel: ['.msgbuttonlabel', 'textContent'],
  msg: ['.message', 'href'],
};

function get(target) {
  var data = target.dataset.tpl;

  if (~queue.indexOf(target)) return;
  queue.push(target);
  target.classList.add('loading');
  request
    .get(target.href)
    .then(JSON.parse)

  // Success
  .then(function(res) {
    if (res.status == 1) {
      notices.set(res.translatedMessage);
    }

    return res;
  })

  .then(function(res) {
    if (res.status != 1 && res.status != 2) {
      notices.set(res.translatedMessage, 'warn');
    }

    return res;
  })

  // Mutual
  .then(function(res) {
      if (res.status == 2) {
        var data = JSON.parse(target.dataset.tpl);
        modal.set(populate(template, datamap, data));
      }

      return res;
    })
    .then(function(res) {
      queue.splice(queue.indexOf(target, 1));
      target.removeAttribute('href');
      target.dataset.interest = res.status;
      target.classList.remove('loading');
    })

  // Errors
  .catch(function() {
    console.log('catch', arguments, this);
    notices.setError();
  });

}

function click(e) {
  var target = e.target.closest('.card a[data-interest]');
  if (target) {
    e.preventDefault();
    e.stopPropagation();
    if (!parseInt(target.dataset.interest)) get(target);
  }

}

document.addEventListener('click', click, true);
