'use strict';

//var preventscroll = require('./preventscroll');

var html = document.documentElement;
var main = document.querySelector('main');

//preventscroll(document.querySelector('#menu'));

// Listen for any links that open the menu
document.addEventListener('click', function(e) {
  var target = e.target.closest('a');
  if (target && target.hash == '#menu') {
    e.preventDefault();
    e.stopPropagation();
    target.setAttribute('aria-expanded', true);
    html.classList.toggle('menu-open');
  }
}, true);

// Set aria-expanded state to false for the link that opened the menu
(new MutationObserver(function(records) {
  var state = records[0].target.classList.contains('menu-open');
  if (!state) {
    var target = document.querySelector('[aria-controls=menu][aria-expanded=true]');
    if (target) target.setAttribute('aria-expanded', state);
  }

})).observe(html, {attributes: true, attributeFilter: ['class']});

// Close the menu when clicking on a link, or the menu backdrop
document.getElementById('menu').addEventListener('click', function(e) {
  var target = e.target.closest('a');
  if ((target && target.href) || e.target == this) {
    html.classList.remove('menu-open');
  }
  if (target && target.hash) {
    e.preventDefault();
  }
});

// Toggle the activity submenu
var menuActivityButton = document.querySelector('[aria-controls=menu-activity]');
if (menuActivityButton) menuActivityButton.addEventListener('click', function(e) {
  var target = document.getElementById('menu-activity');
  var state = target.getAttribute('aria-expanded');
  e.preventDefault();
  target.setAttribute('aria-expanded', !(state === 'true'));
});
