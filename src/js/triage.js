(function(){

function findDuplicateIds() {
  var nodes = document.querySelectorAll('[id]');
  var dupes = Array.prototype.slice.call(nodes).filter(function(node) {
    return ~this.indexOf(node.id) || (this.push(node.id), false);
  },[]);

  if(dupes.length) console.warn('duplicate id', dupes)
}

function init() {
  findDuplicateIds();
}

document.addEventListener('DOMContentLoaded', init);

})();
