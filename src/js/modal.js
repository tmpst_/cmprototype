'use strict';

var preventscroll = require('./preventscroll');

var html = document.documentElement;
var modal = document.getElementById('modal');
var modalcontent = modal.firstElementChild;

// preventscroll(modalcontent);

exports.set = function set(content) {
  modalcontent.appendChild(content);
};

exports.destroy = function destroy() {
  var node = modalcontent.firstElementChild;
  if (node) modalcontent.removeChild(node);
};

// Listen for any links that open the modal
document.querySelector('main').addEventListener('click', function(e) {
  var target = e.target.closest('a');

  if (target && target.hash == '#modal') {
    e.preventDefault();
    e.stopPropagation();
    target.setAttribute('aria-expanded', true);
    html.classList.toggle('modal-open');
  }
});

// Set aria-expanded state to false for the link that opened the modal
// And empty the modal content
new MutationObserver(function(records) {
  var state = html.classList.contains('modal-open');

  if (!state) {
    var content = modalcontent.firstElementChild;
    var target = document.querySelector('[aria-controls=modal]');
    if (target) target.setAttribute('aria-expanded', state);
    if (content) {
      exports.destroy();
    }
  }
}).observe(html, {
  attributes: true,
  attributeFilter: ['class'],
});

// Open the modal when the content is updated
new MutationObserver(function(records) {
  if (records[0].addedNodes.length) {
    var nodes = modalcontent.querySelectorAll('img');
    var i = -1;
    var node;
    for (; node = nodes[++i];) node.onerror = function() {
      this.src = 'data:image/svg+xml,<svg xmlns=\'http://www.w3.org/2000/svg\'/>';
    };
  }

  html.classList.toggle('modal-open', records[0].addedNodes.length);
}).observe(modal, {
  childList: true,
  subtree: true,
});

// Close the modal when clicking on a link
modal.addEventListener('click', function(e) {
  var target = e.target.closest('a');
  if (target && (target.href || target.getAttribute('aria-controls'))) {
    html.classList.remove('modal-open');
  }
});
