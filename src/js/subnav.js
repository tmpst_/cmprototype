(function(controls, elements) {
  return;
  var selected = 0;
  var offset = 44;
  var ticking = false;
  var y = 0;
  var html = document.documentElement;

  // @TODO: Add this to DOMContentLoaded listener
  if (elements && controls) {
    //elements = elements.children;
    controls = controls.children;
    controls[0].parentNode.addEventListener('click', click);
    window.addEventListener('scroll', requestTick);
    set(0);
  }

  function set(n, scroll) {
    controls[selected].removeAttribute('aria-selected');
    controls[n].setAttribute('aria-selected', true);
    selected = n;
    if (scroll) html.classList.remove('nav-pinned'), elements[selected].scrollIntoView();
  }

  function requestTick() {
    ticking || (ticking = true, requestAnimationFrame(tick));
  }

  function tick() {
    var r = elements[selected].getBoundingClientRect();
    var n = selected;
    var dy = window.scrollY - y;

    y += dy;

    if ((dy > 0 && r.bottom < offset && elements[++n] || dy < 1 && r.top > 0 && elements[--n]) && n != selected) {
      set(n);
    }

    html.classList.toggle('subnav-pinned', y > offset * 3);

    // hide the subnav when intersecting the article header
    r = elements[selected].firstElementChild.getBoundingClientRect();
    controls[selected].parentNode.parentNode.style.opacity = (r.top < offset && r.top > offset * -2 ? 0 : 1);

    ticking = false;
  }

  function click(e) {
    var target = e.target.closest('a');
    var i = -1;
    e.preventDefault();
    for (; controls[++i] && controls[i] != target;);
    if (i != selected && i != controls.length) set(i, true);
    target.parentNode.parentNode.scrollTop = 0;
    html.classList.toggle('nav-open');
  }

  // Fix img errors
  var imgs = controls[0].parentNode.querySelectorAll('img');
  for (var i = -1, img; img = imgs[++i];) {

    img.onerror = function() {
      this.src = 'data:image/svg+xml,<svg xmlns=\'http://www.w3.org/2000/svg\'/>';
    };
  }

})(document.querySelector('[role=directory]'), document.querySelectorAll('.viewport-active .card'));
