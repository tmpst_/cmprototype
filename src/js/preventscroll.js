'use strict';

module.exports = function(element, set) {
  function preventscroll() {
    document.body.style.overflowY = 'hidden';
    document.body.addEventListener('touchmove', preventBodyScroll);
  }

  function allowscroll() {
    document.body.style.overflowY = '';
    document.body.removeEventListener('touchmove', preventBodyScroll);

  }

  function preventBodyScroll(e) {
  }

  element.addEventListener('touchstart', preventscroll);
  element.addEventListener('touchend', allowscroll);
  element.addEventListener('touchend', function(e) {
    // console.log('element touchmove', e);
  }, true);

  //
  // $('body').on('touchmove', selScrollable, function(e) {
  //   // Only block default if internal div contents are large enough to scroll
  //   // Warning: scrollHeight support is not universal. (http://stackoverflow.com/a/15033226/40352)
  //   if ($(this)[0].scrollHeight > $(this).innerHeight()) {
  //     e.stopPropagation();
  //   }
  // });
  //

  if (set) preventscroll();

};
