'use strict';

var request = require('./request.js');
var cache = require('./cache.js');
var modal = require('./modal.js');

document.addEventListener('click', function(event) {
  var anchor = event.target.closest('a');
  if (!anchor) return;

  // Ignore modal buttons
  if(!anchor.href) return;

  // Ignore javascript links
  if(/javascript/.test(anchor.href)) return;
  // Ignore modified clicks.
  if (event.button !== 0)
    return;
  if (event.altKey || event.ctrlKey || event.metaKey || event.shiftKey)
    return;

  // Ignore links to other sites.
  if (!(~window.location.origin.indexOf(anchor.host)))
    return;

  // Ignore links for other tabs or windows.
  if (anchor.target === '_blank' || anchor.target === '_top')
    return;

  // Ignore hash links on the same page.
  if ((anchor.pathname === location.pathname) && !!anchor.hash)
    return;

  // Ignore links with the data-force-reload attribute.
  if (anchor.dataset.forceReload)
    return;

  // Ignore jqm pages

  if (anchor.dataset.jqmpage) {
    var card = anchor.closest('.card');
    if (card) {
      var loc = window.location;
      loc.hash = card.id;
      window.history.replaceState({}, document.title, loc.href);
    }

    return;
  }

  // Load clicked link.
  event.preventDefault();
  get(anchor);
});

function get(url) {
  var href = url.href || url;
  var context = cache.get(href);

  context.url = href;

  // @TODO: we need to deal with appended content here so
  // we can unload 'prepended' content
  var append = !!(!(url instanceof Location) && url.dataset.append);
  var prepend = !!(!(url instanceof Location) && url.dataset.prepend);

  // if (href === location.href) return;
  // @TODO: Check if we have an existing viewport container
  // @TODO: Check the cache entry, if expired clean out the existing container
  // @TODO: Create a request if required
  var id = (append || prepend) ? document.querySelector('.viewport-active').id : context.id;
  var viewport = beforeRender(id);
  if (context.content && !append) {
    afterRender(context);
  } else {
    request
      .get(href)

    // Parse the response.
    .then(function(res) {
      return new DOMParser().parseFromString(res, 'text/html');
    })

    // Follow any redirects (dev purposes only).
    .then(function(res) {
      var meta = res.querySelector('[http-equiv=refresh]');
      if (meta) {
        var loc = [
          href.replace(/\/$/, ''),
          meta.getAttribute('content').slice(2),
        ].join('/');
        window.location = loc;
      } else {
        return res;
      }
    })

    // Process the response, populate the cache.
    .then(function(res) {
      var title = res.querySelector('.title');
      title = title && title.textContent || res.title;

      return {
        meta: res.querySelectorAll('meta[id]'),
        scripts: res.querySelectorAll('script'),
        title: title,
        header: res.querySelector('header'),
        subnav: res.querySelector('.subnav'),
        modal: res.querySelector('.modal'),
        content: res.querySelector('.content'),
        href: href,
      };
    })

    // Dev only clean up content
    .then(function(res) {

      var nodes = res.content.querySelectorAll('script, header, .subnav, .modal');
      var i = -1;
      var node;
      for (; node = nodes[++i];) node.parentNode.removeChild(node);
      return res;
    })

    // Dev Only allow one profile-nav in viewport-active
    .then(function(res) {
      var nodes = res.content.querySelectorAll('.profile-nav');
      var profilenav = document.querySelector('.viewport-active').querySelector('.profile-nav');
      var i = profilenav ? -1 : 0;
      var node;
      for (; node = nodes[++i];) res.content.removeChild(node);
      return res;
    })

    // Dev only remove scripts that are already loaded
    .then(function(res) {
      var scripts = [];
      for (var i = -1, script; script = res.scripts[++i];) {
        if (script.id != '__bs_script__' && !(/base.js/.test(script.src)) && (script.src || script.textContent))
          scripts.push(script);
      }

      res.scripts = scripts;
      return cache.set(href, res);
    })

    // Render the response.
    .then(function(res) {
      render(viewport, res, append, prepend);
      return res;
    })

    // Catch request errors.
    .catch(function(err) {
      console.log(err);
      throw err;
    });
  }
}

// @TODO: handle enter/exit animations for the viewport
function beforeRender(id) {
  var current = document.querySelector('.viewport-active');
  if (current) current.classList.remove('viewport-active');

  var viewport = document.getElementById(id);
  if (!viewport) {
    viewport = document.createElement('div');
    viewport.id = id;
    viewport.classList.add('viewport');
    document.querySelector('main').appendChild(viewport);
  }
  viewport.classList.add('viewport-active');
  return viewport;
}

function afterRender(context) {
  document.querySelector('.title').textContent = document.title = context.title;
  if (!(context.url instanceof Location)) {

    history.pushState({
      page: context.id,
    }, context.title, context.href);
  }

  var event = document.createEvent('Event');
  event.initEvent('DOMContentLoaded', true, true);
  document.dispatchEvent(event);
}

function render(viewport, context, append, prepend) {
  // meta: res.querySelectorAll('meta[id]'),
  // scripts: res.querySelectorAll('script'),
  // title: res.title || res.querySelector('.title').textContent,
  // navbar: res.querySelector('.navbar'),
  // subnav: res.querySelector('.subnav'),
  // modal: res.querySelector('.modal'),
  // content: res.querySelector('.content'),
  // href: href,

  // var viewports = document.querySelector('.viewports');
  // var viewportActive = viewports.querySelector('.viewport-active');
  // var viewport = document.getElementById(context.id);

  // @TODO: add mutationObserver to limit the number of .content divs
  if (prepend) {
    viewport.insertBefore(context.content, viewport.firstElementChild);
  } else {
    viewport.appendChild(context.content);
  }

  // @TODO: move to afterRender ?
  if (context.header) {
    var node = document.querySelector('header');
    node.parentNode.replaceChild(context.header, node);
  }

  // @TODO: move to afterRender ?
  if (context.subnav) {
    var node = document.querySelector('.subnav');
    node.parentNode.replaceChild(context.subnav, node);
  }

  if (context.scripts) {
    for (var i = -1, script; script = context.scripts[++i];) {
      var node = document.createElement('script');
      ['id', 'src', 'async', 'defer', 'type', 'charset', 'textContent'].forEach(function(prop) {
        if (script[prop]) node[prop] = script[prop];
      });

      //@TODO: Do we want to append to the viewport or the document?
      viewport.appendChild(node);
    }
  }

  if (context.modal) {
    setTimeout(function() {
      modal.set(context.modal);
    }, 500);
  }

  afterRender(context);
}

window.addEventListener('popstate', function() {
  if (history.state) get(window.location);
});

// Load the page content via XHR
// requesting a page via XHR sends the Accept: vnd.cupidmedia.html header
// the response is a 'partial payload'
function init() {

  var viewport = document.querySelector('.splash');

  if (viewport.firstElementChild) {
    viewport.id = cache.get(location.href).id;
    var content = document.querySelector('.modal');
    if (content) modal.set(content);
  } else {
    get({
      href: location.href,
      dataset: {},
    });
  }

  window.removeEventListener('DOMContentLoaded', init);
}

window.addEventListener('DOMContentLoaded', init);
