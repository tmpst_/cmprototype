'use strict';

// jscs:disable requireSpacesInForStatement
var transformProp = 'transform' in document.body ? 'transform' : '-webkit-transform';
var slider;
var target;
var scrolling;
var sx = 0;
var dx = 0;
var absdx = 0;
var current = -1;
var offset = 0;
var rtl = (document.documentElement.getAttribute('dir') == 'rtl');

function set(n) {
    current = Math.max(0, Math.min(n, slider.children.length - 1));
    var factor = rtl ? 100 : -100;
    transform(current * factor + 'vw');
    var nav = slider.parentNode.querySelector('nav');
    if (nav) for (var i = -1, node; (node = nav.children[++i]);) {
      node.classList.toggle('active', i == current);
    }
  }

function transform(x) {
    slider.style[transformProp] = 'translateX(' + x + ') translateZ(0)';
  }

function touchstart(e) {
    target = e.target.closest('figure');
    if (target) {
      slider = target.parentNode;
      if (slider.children.length > 1) {
        offset = slider.getBoundingClientRect().left;
        current = 0;
        scrolling = true;
        while (slider.children[current] != target) ++current;
        sx = e.touches[0].pageX;
        slider.addEventListener('touchmove', touchmove);
        slider.style.transition = 'none';
      }
    }
  }

function touchmove(e) {
    dx = e.touches[0].pageX - sx;
    if (scrolling && Math.abs(dx) > target.offsetWidth / 6) {
      scrolling = false;
      sx = e.touches[0].pageX;
      dx = 0;
    }

    if (!scrolling) {
      e.preventDefault();
      transform(parseInt(offset + dx, 10) + 'px');
    }

  }

function touchend(e) {
    if (target) {
      slider.style.transition = '';
      slider.removeEventListener('touchmove', touchmove);

      var factor = rtl ? (dx > 0) : (dx < 0);
      current = (Math.abs(dx) < target.offsetWidth / 4) ? current : factor ? current + 1 : current - 1;
      set(current);
    }
  }

document.addEventListener('touchstart', touchstart);
document.addEventListener('touchend', touchend);

// jscs:enable requireSpacesInForStatement
