'use strict';
var stickyfill = require('stickyfill')();

// jscs:disable requireSpacesInForStatement
window.addEventListener('DOMContentLoaded', function() {
  for (var i = -1, nodes = document.getElementsByClassName('sticky'), node; (node = nodes[++i]);) {
    stickyfill.add(node);
  }
});

// (function(el) {
//   var ticking = false;
//   var y = 0;
//
//   function tick(timer) {
//     var dy = window.scrollY - y;
//     y += dy;
//     Math.abs(dy) > 20 && el.classList.toggle('stick', dy > 0);
//     ticking = false;
//   }
//
//   window.addEventListener('scroll', function (){
//     ticking || (ticking = true, requestAnimationFrame(tick));
//   });
//
// })(document.querySelector('main'))

// jscs:enable requireSpacesInForStatement
