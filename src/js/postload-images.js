'use strict';
var inviewport = require('in-viewport');

module.exports = function(selector, root) {
  root = root || document;

  // jscs:disable requireSpacesInForStatement
  for (var i = -1, nodes = root.querySelectorAll(selector), node; (node = nodes[++i]);) {
    inviewport(node, function postloadImage(node) {
      var image = node.querySelector('[data-src]');
      if (image) {
        image.setAttribute('src', image.dataset.src);
        image.addEventListener('load', function() {
          image.removeAttribute('data-src');
        });
      }
    })
  }

  // jscs:enable requireSpacesInForStatement
}
