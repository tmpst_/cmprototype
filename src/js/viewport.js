

// Create a viewport given an id
function create(id) {
  var viewport = document.createElement('div');
  viewport.id = id;
  return viewport;
}

// Remove
function destroy(id) {
  var viewport = document.getElementById(id);
  return viewport.parentNode.removeChild(viewport);
}
