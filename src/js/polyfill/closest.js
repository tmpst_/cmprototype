(function(ELEMENT) {
  ELEMENT.matches = ELEMENT.matches || ELEMENT.mozMatchesSelector || ELEMENT.msMatchesSelector || ELEMENT.oMatchesSelector || ELEMENT.webkitMatchesSelector;

  ELEMENT.closest = ELEMENT.closest || function closest(selector) {
    var _this = this;

    while (_this) {
      if (_this.matches(selector)) {
        break;
      }

      _this = _this.parentElement;
    }

    return _this;
  };
}(Element.prototype));
