// Example
//
// html
// ====
// <form id="myform">....</form>
//
// JS
// ==
//
// var validate = require('./validate');
// var request = require('./request');
//
// var myform = document.getElementById('myform');
//
// validate.set(myform, function(event) {
//   // this, event.target = myform
//   // event.type = 'submit'
//   request.post(this.action, new FormData(this))
//     .then(function(res){
//       // do stuff
//     })
//
// });

// Suppress default validation ui
function onInvalid(event) {
  event.preventDefault();
}

// Check field validity
function onInput(event) {
  var field = event.target;
  var valid = field.validity.valid;

  field.parentNode.setAttribute('aria-invalid', !valid);
  field.nextElementSibling.textContent = valid ? '' : field.title;
}

// Disable the submit button when the form is invalid
function toggleSubmit(btn, event) {
  var action = this.checkValidity() ? 'remove' : 'set';
  btn[action + 'Attribute']('disabled', '');
}

// Prevent submitting the form and call the callback function
function onSubmit(fn, event) {
  event.preventDefault();
  if (this.checkValidity()) return fn.call(this, event);
}

// Attach event listeners to the form
function set(form, fn) {
  form.setAttribute('novalidate', '');
  form.addEventListener('invalid', onInvalid);
  form.addEventListener('input', onInput);
  form.addEventListener('input', toggleSubmit);
  form.addEventListener('submit', onSubmit.bind(form, fn));
}

// Remove event listeners from the form
function destroy(form, fn) {
  form.removeAttribute('novalidate');
  form.removeEventListener('invalid', onInvalid);
  form.removeEventListener('input', onInput);
  form.removeEventListener('input', toggleSubmit.bind(form, form.querySelector('[type=submit]')));
  form.removeEventListener('submit', onSubmit.bind(form, fn));
}

module.exports = {
  set: set,
  destroy: destroy,
};
