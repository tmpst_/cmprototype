var html = document.documentElement;
var ticking = false;
var y = 0;

function requestTick() {
  ticking || (ticking = true, requestAnimationFrame(tick));
}

function tick() {
  var dy = window.scrollY - y;
  y += dy;
  if (dy > 1 && y > 44) html.classList.remove('nav-pinned');
  if ((dy < -10 && dy > -44) || y < 10) html.classList.add('nav-pinned');
  ticking = false;
}

html.classList.add('nav-pinned');

window.addEventListener('scroll', requestTick);
