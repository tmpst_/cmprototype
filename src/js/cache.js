'use strict';

var cache = {};

// Convert a string to a 32 bit integer
var hash = exports.hash = function(str) {
  var hash = 0;
  if (str.length == 0) return hash;
  for (var i = 0, len = str.length, chr; i < len; i++) {
    chr   = str.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0;
  }

  return hash;
};

// Setter
exports.set = function(id, data, expire) {
  var date = new Date();
  id = ('' + id === id) ? hash(id) : id;

  // expire = expire || date.setTime(date.getTime() + 3e5);
  expire = 0;
  data.id = id;
  data.expires = expire;
  cache[id] = data;
  return data;
};

// Getter
exports.get = function(id) {
  id = ('' + id === id) ? hash(id) : id;
  var data = cache[id];
  if (data && data.expires > Date.now()) {
    delete cache[id];
  }

  if (!data) {
    data = {id: id};
  }

  return data;
};
