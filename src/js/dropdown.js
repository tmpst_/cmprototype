'use strict';

function click(e) {
  var nodes = document.querySelectorAll('[aria-controls=' + this.id + ']');
  var i = -1;
  var node;
  for (; node = nodes[++i];) {
    node.setAttribute('aria-pressed', false);
    node.setAttribute('aria-expanded', false);
  }

  this.setAttribute('aria-hidden', true);
  this.removeEventListener('click', click);
}

document.addEventListener('click', function(e) {
  var target = e.target.closest('[aria-pressed][aria-controls]');
  if (target) {
    var node = document.getElementById(target.getAttribute('aria-controls'));
    e.preventDefault();
    if (node) {
      var state = node.getAttribute('aria-hidden') == 'true';
      target.setAttribute('aria-expanded', state);
      target.setAttribute('aria-pressed', state);
      node.setAttribute('aria-hidden', !state);
      node.addEventListener('click', click);
    }

  }

});
