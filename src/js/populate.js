/**
 *  Populate a template - returns a rendered template that can be inserted into the dom.
 *
 * @param template Element - a template element
 * @param datamap Object Literal - {key: [selector, attribute]} map data to a selector and attribute to populate
 * @param data Object Literal - the data to insert into the template
 * @returns tpl DocumentFragment - the rendered template
 */

module.exports = function(template, datamap, data) {
  var tpl = document.importNode(template.content, true);
  Object.keys(datamap).forEach(function(key) {
    for (var i = -1, node, nodes = tpl.querySelectorAll(datamap[key][0]); node = nodes[++i];) {
      var attr = datamap[key][1];
      if (attr == 'class') {
        node.classList.add(data[key]);
      } else {
        node[attr] = data[key];
      }
    }
  });

  return tpl;
}
