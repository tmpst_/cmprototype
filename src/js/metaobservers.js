'use strict';

// MutationObserver options listens to changes to meta.content
var options = {attributes: true, attributeFilter: ['content']};

// meta tag containing uptodate activity counts. (JSON encoded object literal)
// {
//   "activity-listInteractionTotal": "0",
//   "activity-interest": "0",
//   "activity-favourite": "0",
//   "activity-profileview": "0",
//   "activity-mailTotal": "0"
// }
var metaActivity = document.getElementById('activity');

// meta tag containing uptodate 'online' data (json encoded array of id's)
var metaOnline = document.getElementById('online');

// JSON encode content and store it on a <meta> element with the given id
exports.set = function(id, content) {
  return document.getElementById(id).content = JSON.stringify(content);
};

// Decode JSON stored on a <meta> element given an id
exports.get = function(id) {
  var content = document.getElementById(id).content;
  return content ? JSON.parse(content) : '';
};

// Set a bool value for dataset attribute given a <meta> element id
// <meta id="foo" content="["123"]">
// <div data-profileId="123" data-foo="true">
function applyDatasetValue(records) {
  var target = Array.isArray(records) ? records[0].target : records;
  var data = target.content ? JSON.parse(target.content) : [];
  var nodes = document.querySelector('.viewport-active').querySelectorAll('[data-profileid]');
  var i = -1;
  var node;
  for (; node = nodes[++i];) {
    node.dataset[target.id] = data.indexOf(node.dataset.profileid) != -1;
  }
}

function applyActivityData() {
  var data = metaActivity.content ? JSON.parse(metaActivity.content) : {};
  Object.keys(data).forEach(function(key) {
    for (var i = -1, node, nodes = document.getElementsByClassName(key); node = nodes[++i];) {
      var count = parseInt(data[key]);
      if (count > 0) {
        node.dataset.count = count;
      } else {
        node.removeAttribute('data-count');
      }
    }
  });
}

new MutationObserver(applyActivityData).observe(metaActivity, options);
new MutationObserver(applyDatasetValue).observe(metaOnline, options);
document.addEventListener('DOMContentLoaded', applyActivityData);
