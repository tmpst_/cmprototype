'use strict';

// jscs:disable requireSpacesInForStatement
var slider = require('./slider');
var cache = require('./cache');

// var node = document.querySelector('.viewport-active');
// var postload = node.getElementsByClassName('postload');
var node;
var postload;
var loadmore;
var doloadmore;
var html = document.documentElement;
var offset = window.innerHeight / 2;
var lastScrollY = 0;

function inviewport(el, x, y) {
  var r = el.getBoundingClientRect();
  x = x || 0;
  y = y || 0;
  return (!!r
    && r.bottom > 0
    && r.right > 0
    && r.top < html.clientHeight + y
    && r.left < html.clientWidth + x
  );

}

// @TODO: update this to queue the image loading, rather than actually load it.
function loadimg(img) {
  img.src = img.dataset.src;
  img.onload = function() {
    img.removeAttribute('data-src');
    if (img.naturalWidth > img.naturalHeight) img.style.height = '100%';
  };

  img.onerror = function() {
    img.src = "data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg'/>";
  };

}

function loadnext(anchor) {
  var event = document.createEvent('MouseEvents');
  event.initEvent('click', true, true);
  anchor.dispatchEvent(event);

  anchor.style.display = 'none';
}

// Load images upon user interaction with the slideshow
var slide = new MutationObserver(function(records) {
  records.filter(function(record) {
    var target = record.target;
    return target.parentNode.matches('.slides') && inviewport(target);
  }).map(function(record) {
    for (var i = -1, img, nodes = record.target.querySelectorAll('[data-src]');
    img = nodes[++i];) {
      if (!img.src) loadimg(img);
    }

    return record;
  });
});

// Lazy load the card
var card = new MutationObserver(function(records) {
  records.filter(function(record) {
    return record.target.matches('article.card');
  }).map(function(record) {
    var nodes = record.target.querySelectorAll('.card-avatar [data-src], figure:first-of-type [data-src]');
    for (var i = -1, img; img = nodes[++i];) if (!img.src) loadimg(img);
    return record;
  });

});

//
var ticking = false;

function requestTick() {
  if (!ticking) {
    requestAnimationFrame(tick);
    ticking = true;
  }
}

function tick() {

  if (postload) for (var i = -1, node; (node = postload[++i]);) {

    if (inviewport(node, 0 - offset, offset)) {
      node.classList.remove('postload');
    }
  }

  // Loadmore buttons
  var y = window.scrollY - lastScrollY;;
  lastScrollY += y;
  if (loadmore && doloadmore) for (var i = -1, node; (node = loadmore[++i]);) {
    // Eliminate this;
    var el = document.getElementById(cache.get(node.href));
    if (!el) {
      var v;
      if (y > 0) {
        v = inviewport(node, 0, offset);
      }

      if (y < 0) {
        v = inviewport(node, 0 - offset, 0);
      }
      console.log('load', v, y, lastScrollY)
      if (v) loadnext(node);
    }
  }

  ticking = false;
}

window.addEventListener('scroll', requestTick);

document.addEventListener('DOMContentLoaded', function() {
  node = document.querySelector('.viewport-active');
  postload = node.getElementsByClassName('postload');
  loadmore = node.getElementsByClassName('loadmore');
  slide.observe(node, {attributeFilter: ['style'], attributes: true, subtree: true});
  card.observe(node, {attributeFilter: ['class'], attributes: true, subtree: true});
  requestTick();

  // Fix a race condition??
  // setTimeout(tick, 0);
  requestAnimationFrame(tick);

  //Attempt to reposition the viewport after layout
  setTimeout(function() {
    doloadmore = true;
    if (lastScrollY > 0) {
      var node = document
        .querySelector('[id="' + window.location.hash + '"]');

      if (node) {
        node.scrollIntoViewIfNeeded(false);
      } else {
        console.warn('%s doesn\'t exist in the document', window.location.hash);

      }
    } else if (lastScrollY < 0) {
      var h = document
        .querySelector('.viewport-active').querySelector('.content')
        .getBoundingClientRect().height;
      lastScrollY = h + 44;
      window.scrollTo(0, lastScrollY);
    }

  }, 48);

  // Hack to prevent loading the next page when returning from jqm pages
  // setTimeout(function() {
  //   doloadmore = true;
  // }, 500);
});

// jscs:enable requireSpacesInForStatement
