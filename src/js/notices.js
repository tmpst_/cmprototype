var html = document.documentElement;
var notices = document.getElementById('notices');
var noticescontent = notices.firstElementChild;
var defaultmessage = document.getElementById('defaultmessage').content;
var timer;
var typemap = {
  error: 'bg-red',
  warn: 'bg-yellow',
  confirm: 'bg-green',
};

var classes = Object.keys(typemap).map(function(i) { return typemap[i]; }).join(',');

exports.setError = function(message) {
  exports.set(message || defaultmessage, 'error');
};

// @TODO: Refactor set / destroy to listen to transitionEnd on dov.notices
exports.set = function(text, type) {
  exports.destroy();
  setTimeout(function() {
    html.classList.add('notices-open');
    noticescontent.textContent = text;
    noticescontent.classList.add(typemap[type || 'confirm']);
    timer = setTimeout(exports.destroy, 4000);
  }, 500);
};

exports.destroy = function() {
  html.classList.remove('notices-open');
  setTimeout(function() {
    noticescontent.textContent = '';
    noticescontent.classList.remove(classes);
  }, 500);

  clearTimeout(timer);
};

notices.addEventListener('click', exports.destroy);
