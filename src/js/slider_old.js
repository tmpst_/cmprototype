// jscs:disable requireSpacesInForStatement
var transformProp = 'transform' in document.body ? 'transform' : '-webkit-transform';
function slider(node) {
  var slider = node.firstElementChild;
  var nav = node.lastElementChild;
  var slides = slider.children;
  var current = 0;
  var width = 0;

  if (slides.length == 1) return;

  function set(n) {
    nav.children[current].classList.remove('active');
    current = Math.max(0, Math.min(n, slides.length - 1));
    nav.children[current].classList.add('active');
    transform(current * width * -1);
  }

  function transform(x) {
    var transform = 'translateX(' + x + 'px) translateZ(0)';
    for (var i = -1, slide;
      (slide = slides[++i]);) {
      slide.style[transformProp] = transform;
    }
  }

  function resize() {
    width = node.getBoundingClientRect().width;
    for (var i = -1, slide;
      (slide = slides[++i]);) {
      slide.style.left = width * i + 'px';
      if (i == current) set(i);
    }
  }

  if ('ontouchstart' in window) {
    (function() {
      var sx = 0;
      var sy = 0;
      var dx = 0;
      var dy = 0;
      var offset = 0;
      var temper = 70;
      var absdx = 0;

      function touchstart(e) {
        var touch = e.touches[0];
        var target = e.target.closest('figure');
        for (var i = -1, slide;
          (slide = slides[++i]);) {
          slide.style.transition = 'none';
        }

        offset = parseInt(target.style.left, 10) * -1;
        sx = touch.pageX;
        sy = touch.pageY;
      }

      function touchmove(e) {
        var touch = e.touches[0];
        dx = sx - touch.pageX;
        dy = sy - touch.pageY;
        absdx = Math.abs(dx);
        if (absdx > Math.abs(dy)) {
          e.preventDefault();
          transform(offset - dx);
        }

      }

      function touchend(e) {
        var target = e.target.closest('figure');
        for (var i = -1, slide; (slide = slides[++i]);) slide.style.transition = '';
        var i = (absdx < temper) ? current : (dx < 0) ? current - 1 : current + 1;
        set(i);
      }

      slider.addEventListener('touchstart', touchstart);
      slider.addEventListener('touchmove', touchmove);
      slider.addEventListener('touchend', touchend);

    })();

  } else {
    nav.addEventListener('click', function click(e) {
      var target = e.target;
      if (target != nav) {
        for (var i = -1, child;
          (child = nav.children[++i]);)
          if (child == target) {
            return set(i);
          }
      }
    });

  }

  ['resize', 'orientationchange'].map(function(i) {
    window.addEventListener(i, resize);
  });

  resize();
}

module.exports = slider;

// jscs:enable requireSpacesInForStatement
