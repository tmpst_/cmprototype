# Installation

Preliminary note: Windows users need to ignore the "$" character in the shell commands below.
 
1. [Download](https://nodejs.org/download/) and install `node` and `npm`. If you are on Windows, download the MSI and just follow the prompts for a standard installation.

2. [Download](https://www.python.org/downloads/) and install `python` (the latest of the 2.7.x line). On Windows, grab the MSI and follow the standard installation steps, Mac users have python installed by default. 

3. Make sure the installation directory of `python` is on your system PATH variable.

4. Open up a terminal window in the prototype folder and install the grunt commandline interface

	`$ npm install grunt-cli -g`

5. Install the prototype application

	`$ npm install`

6. Run the application

	`$ npm start`

7. Starting the application should load the [application](http://localhost:3000) in your default browser. You can manage aspects of the server from the [admin ui](http://localhost:3001), such as how to connect to it from your phone.

8. Editing any of the files in the `src` folder should [automatically update](http://localhost:3000) any [connected clients](http://localhost:3001).


# Technologies

The prototype application leverages many of the same technologies we will use in the production. Below are the starting points:

- [Browsersync](http://browsersync.io)
- [CSSNext](http://cssnext.io)
- [Basscss](http://basscss.com)
- [Handlebars](http://handlebarsjs.com)
- [RatchetJS](http://goratchet.com)
