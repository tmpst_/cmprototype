var url = require('url');
var gzipstatic = require('connect-gzip-static')('./public');

module.exports = {
  files: ['public/**'],
  server: {
    baseDir: 'public',
    middleware: [
      function(req, res, next) {
        var xhr = /vnd\.cupidmedia/.test(req.headers.accept);
        var html = /text\/html/.test(req.headers.accept);

        // Return the full payload for non-xhr requests for text/html
        if (html) {
          req.url = '/full-payload.html';
        }

        // Force gzip encoding for html and xhr
        if (html || xhr) {
          req.headers['accept-encoding'] = 'gzip';
        }
        next();
      },

      function(req, res, next) {
        // Handle POST
        if (req.url == '/api/form.html') {
          res.end('<div class="modal"><p>Hello world</p></div>');
        }

        next();
      },

      function(req, res, next) {
        req.url = req.url.replace(/messages\/\d+/, '/messages/');
        next();
      },

      function(req, res, next) {
        // Direct all requests to the 'express interest' api
        var responses = [
          'api/interest1.json',
          'api/interest2.json',
          'api/interest-1.json',
        ];
        var response = responses[Math.floor(Math.random() * responses.length)];
        req.url = req.url.replace(/api\/interest\/\d+/, response);
        // Direct all request to /messages/id to messages
        req.url = req.url.replace(/messages\/\d+/, '/messages/');
        next();
      },

      gzipstatic,

    ],
  },
  port: 3000,
  open: false,
  ui: {
    port: 3001,
  },

  // ghostMode: {
  //   clicks: true,
  //   forms: true,
  //   scroll: true,
  // },
  ghostMode: false,

};
