// Copy platform specific metalsmith.json
// This is just to work around a bug in the metalsmith-metadata package
// Remove this process once that bug is fixed.
// See https://github.com/segmentio/metalsmith-metadata/pull/11

var fs = require('fs');
var src = (process.platform === 'win32') ? 'metalsmith.win.json' : 'metalsmith.json';

fs.readFile('./config/' + src, 'utf8', function(err, data) {
  if (err) throw err;
  console.log('platform', process.platform);
  fs.writeFile('metalsmith.json', data);
});
